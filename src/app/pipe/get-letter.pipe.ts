import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "getLetter"
})
export class GetLetterPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    //var _urlImage = "../images/flags/flag_";
    let arrayFlag = ["arg","chi","par","bol","col","per","bra","ecu","uru","cat","jap","ven","non"];
    let flag = (value != null) ? value.substr(0, 3).toLowerCase() : '';
    let posBack = 32 * ((arrayFlag.indexOf(flag) !== -1) ? arrayFlag.indexOf(flag) : arrayFlag.length);
    console.log(posBack);
    arrayFlag.push(flag);
    return posBack;

    //return console.log("hola" , value );
  }
}
