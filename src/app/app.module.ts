import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ServicesService } from './services/services.service';
import { HttpModule } from '@angular/http';
import { GetLetterPipe } from './pipe/get-letter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    GetLetterPipe
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [ServicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
