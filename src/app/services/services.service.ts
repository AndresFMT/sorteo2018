import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()

export class ServicesService {
	url: string;
  constructor(private http: Http) {

   }

  getService(url: string): Observable<any> {
  	return this.http.get(url)
  		.map(
  			(response: Response) => {
					return response.json();
  			}
  		);
  }
}
