import { Component } from "@angular/core";
import { ServicesService } from "./services/services.service";
import { Response } from "@angular/http";
import { Equipos } from "./models/equipo.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.sass"]
})
//https://tomalo.herokuapp.com/

export class AppComponent {
  url = 'https://tomalo.herokuapp.com/https://static.caracoltv.com/sorteocopaamerica.json';
  title = "app";
  keys = [];
  print = false;
  groups = {
    A: new Array(Equipos, Equipos, Equipos, Equipos),
    B: new Array(Equipos, Equipos, Equipos, Equipos),
    C: new Array(Equipos, Equipos, Equipos, Equipos)
  };
  constructor(private copaRes: ServicesService) {}

  ngOnInit() {
    this.restJson();
    setInterval(() => {
      this.restJson();
    }, 100000);
    for (let key in this.groups) {
      this.keys.push(key);
    }
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }
  restJson() {
    this.copaRes.getService(this.url).subscribe(res => {
      let teams = res.field_sorteo_copa_am_rica;
      this.addTeam(teams);
    });
  }
  addTeam(teams) {
    for (let key in teams) {
      let pos = parseInt(teams[key].field_orden) - 1;
      this.groups[teams[key].field_grupo_copa][pos] = teams[key];
      if (this.groups[teams.field_paises_copa] == "colombia") {
  
      }
    }
  }
}
